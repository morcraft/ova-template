const _ = require('lodash')
const fs = require('fs')
const chalk = require('chalk')
const log = console.log
const ignoredFiles = require('./ignoredFiles.js')
const path = require('path')

module.exports = function(files, targetPath){
    if(typeof targetPath !== 'string'){
        throw new Error('A target path was expected as a string.')
    }
    if(!_.isArray(files)){
        throw new Error('Expected an array of files to ignore. Received ' + typeof files);
    }

    var string = ignoredFiles;
    _.forEach(files, function(v, k){
        string += '\n' + v;
    })

    //console.log(files)

    fs.writeFileSync(path.join(targetPath, '.gitignore'), string);
    log(chalk`{green Successfully wrote .gitignore}`)
}