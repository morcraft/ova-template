/* ¡No modificar! */

'use strict';
const $                    = require('jquery');
const animatePerSlide      = require('animate-per-slide')
const ovaProgress          = require('ova-progress')
const stageResize          = require('stage-resize')
const presentation         = require('presentation')
const ovaConcepts          = require('ova-concepts')
const _                    = require('lodash')
const Hammer               = require('hammerjs')
const courseData           = require('courseData.json')
const concepts             = require('concepts.json')
const isMobile             = require('is-mobile')
const preciseDraggable     = require('precise-draggable')
const ovaNavigationButtons = require('ova-navigation-buttons')
const ovaTeam              = require('ova-team')
const swal                 = require('sweetalert2')
const swalOptions          = require('swalOptions.js')
const API                  = require('ova-api')
const ovaPreload           = require('ova-preload')
const ovaTopicsFunctions   = require('ova-topics-functions')
const ovaConfig            = require('ovaConfig.json')
const imageSources         = require('imageSources.json')
const fontSources          = require('fontSources.json')
const topics               = require('topics.json')
const preloadSpinner       = require('preloadSpinner.js');
const staticTemplates      = require('staticTemplates.js');
const slideHooks           = require('slideHooks.js');
const utils                = require('utils.js');
const ovaCourseActivities  = require('ova-course-activities');
const finishOva            = require('finishOva.js')
const ovaTooltip           = require('ova-tooltip')

const app = function() {
    var self = this;
    self.APIInstance = new API();
    self.preloader = new ovaPreload();
    self.ovaProgress = new ovaProgress({
        courseData: courseData,
        topics: topics
    });

    preloadSpinner.updateMessage('Escalando escenario...');
    
    //Se escala el escenario al finalizar la carga de la ventana
    stageResize.resizeStage();
    
    //Se añaden eventos de auto-escala en evento de redimensionamiento en ventana
    stageResize.addWindowResizeEvents();

    //Algunos slides requieren el estado de esta variable
    //Se renderizan los templates en \partials
    staticTemplates.renderPartials(courseData);
    
    preloadSpinner.updateMessage('Obteniendo información del usuario...');

    self.APIInstance.makeRequest({
        component: 'utils',
        method: 'getAllUsefulData',
        showFeedback: true,
        arguments: {
            shortName: courseData.shortName,
            unit: courseData.unit
        }
    })
    .then(function(response){
        self.initResponse = response;
        preloadSpinner.updateMessage('Cargando imágenes...');
        return self.preloader.fetchSources({
            sourceType: 'image',
            sources: imageSources.paths,
            stopOnReject: true,
            onProgress: function(args){
                preloadSpinner.updateMessage('Cargando recurso visual [' + _.size(args.loadedSources) + '/' + _.size(args.sources) + ']');
            },
            onFail: function(args){}
        }) 
    })
    .then(function(loadedImages){
        preloadSpinner.updateMessage('Cargando fuentes...');

        return self.preloader.fetchSources({
            sourceType: 'font',
            sources: fontSources.families,
            stopOnReject: true,
            options: {
                timeout: 360000//6 min
            },
            onProgress: function(args){
                preloadSpinner.updateMessage('Cargando fuente [' + _.size(args.loadedSources) + '/' + _.size(args.sources) + ']');
            },
            onFail: function(args){}
        })
    })
    .then(function(response){
        var slidesArguments = {};

        if(_.isObject(self.initResponse.courseActivities)){
            self.ovaCourseActivities = new ovaCourseActivities({
                data: self.initResponse.courseActivities.response
            });

            self.initResponse.courseActivities = {
                response: self.ovaCourseActivities.data
            }
        }

        _.forEach(self.initResponse, function(v, k){
            slidesArguments[k] = v.response;
        });

        self.slidesConfig = _.merge({
            slidesArguments: slidesArguments,
            $target: presentation.getPresentationElement(),
        }, ovaConfig.slidesConfig)

        //Se requiere módulo que permite renderizar los templates estáticos almacenados en /partials  
        staticTemplates.renderSlides(self.slidesConfig);

        //Se inicializa la presentación
        presentation.init();

        //Se inicializan los botones de navegación
        ovaNavigationButtons.initializeNavigationButtons({
            $target: stageResize.currentProps.$stage
        });

        //Se inicializan los créditos del OVA
        ovaTeam.initializeTeamIcon({
            $target: $('.ova-content-footer')
        });

        //Se inicializa el modal que se desprende de los créditos
        ovaTeam.initializeTeamModal({
            $target: stageResize.currentProps.$stage
        });

        if(_.isString(self.initResponse.team.response)){
            try{
                self.initResponse.team.response = JSON.parse(self.initResponse.team.response);
            }
            catch(error){
                var local = 'team.json';
                self.initResponse.team.response = require(local);
            }
        }

        //Se renderiza el arbol de créditos en el DOM
        ovaTeam.renderTeamTree({
            $target: ovaTeam.currentProps.$teamModal.find('.body'),
            team: self.initResponse.team.response
        });

        //Se requiere la función en el módulo utils y se ejecuta con las propiedades
        //actuales del OVA
        utils(ovaConfig)

        staticTemplates.addEventListeners({
            slidesConfig: self.slidesConfig,
            initResponse: self.initResponse,
            APIInstance: self.APIInstance,
            ovaProgress: self.ovaProgress,
            ovaConcepts: ovaConcepts
        });

        //Se añaden los eventos que se desprenden
        //del cambio entre slides en diapositivas
        slideHooks.addHooks();

        //Se inicializan los conceptos obtenidos
        ovaConcepts.initializeConcepts({
            $target: $('.learned-concepts')
        });

        ovaConcepts.currentProps.concepts = _.pick(concepts, _.keys(self.initResponse.concepts.response));
        ovaConcepts.refreshConcepts();

        //Se inicializa la barra de progreso
        self.ovaProgress.renderProgressBar({
            $target: $('.ova-content-header')
        });

        self.ovaProgress.topics.finished = self.initResponse.finishedTopics.response;
        self.ovaProgress.setProgressBarAmount();

        preloadSpinner.hide();
        $('#wrapper').show();
        stageResize.resizeStage();

        self.ovaProgress.updated.add(function(state){
            self.tippy.destroyAll()
            self.ovaProgress.updateVisualClue()
            self.tippy = ovaTooltip()
            if(_.size(state.pending()) === 0 && !self.finishedOva){
                finishOva();
                self.finishedOva = true;
            }
        });

        if(_.size(self.ovaProgress.topics.pending()) === 0){
            finishOva();
            self.finishedOva = true;
        }

        try{
            var addons = 'addOns.js';
            self.addOns = require(addons);
        }
        catch(error){}
    
        if(_.isFunction(self.addOns)){
            self.addOns();
        }

        try{
            var topicsFunctions = 'topicsFunctions.js';
            self.topicsFunctions = require(topicsFunctions);
        }
        catch(error){}
    
        if(_.isObject(self.topicsFunctions)){
            self.ovaTopicsFunctions =  new ovaTopicsFunctions({
                ovaProgress: self.ovaProgress,
                topicsFunctions: self.topicsFunctions
            })
        }

        self.tippy = ovaTooltip()
    })
    .catch(function(error){
        swal({
            type: 'error',
            title: 'Ocurrió un error al cargar el objeto de aprendizaje',
            text: 'Comprueba tu conexión a internet. Si el problema persiste, contacta a un administrador.'
        })

        console.error(error);
        
        preloadSpinner
            .updateMessage(error.message)
            .hideSpinner()
            .updateIcon(':(');
   
    })
}

module.exports = new app();
