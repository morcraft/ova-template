/*  
    ¡No modificar!
    Eventos y funciones útiles que no requieren un módulo individual.
*/
const presentation = require('presentation');
const isMobile = require('is-mobile');
const ovaConcepts = require('ova-concepts');
const _ = require('lodash');
const screenfull = require('screenfull')

module.exports = function (ovaConfig) {
    $(document).on('keydown', function(e){
        if(e.which === 9){
            e.preventDefault()
            e.stopImmediatePropagation()
        }
    })

    if (isMobile()) {
        $('html').addClass('is-mobile');
    }

    $('body').on('click', '.click-me', function () {
        $(this).addClass('clicked')
    })

    $('body').on('mouseenter', '.hover-me', function () {
        $(this).addClass('hovered')
    })

    $('body').on('click', '[data-switch-to-slide]', function () {
        if ($(this).hasClass('disabled')) {
            return false;
        }
        var target = $(this).attr('data-switch-to-slide');
        var directions = ['prev', 'home', 'next'];
        var slide = null;
        if(_.includes(directions, target)){
            slide = target;
        }
        else{
            slide = $('.slide[data-slide="' + target + '"]').eq(0)
        }
        presentation.switchToSlide({
           slide: slide 
        })
    });

    $('.concepts-modal .close-button').on('click', function () {
        $('.concepts-modal').hide()
    });

    $('.secondary-info .glossary').on('click', function () {
        ovaConcepts.showEarnedConceptsModal()
    });

    _.forEach(ovaConfig.slidesConfig.disabledSlides, function (v, k) {
        $('.slide[data-slide="' + v + '"]').data('presentation').enabled = false;
    });

    $('.ova-content-header .fullscreen').on('click', function(){
        if (screenfull.enabled){
            screenfull.toggle();
        }
    })

    $('body').on('mouseenter', '*', function(){
        if($(this).hasClass('click-me') && $(this).hasClass('animated')){
            $(this).removeClass('animated')
        }
    })

    $('.slide .navigation-buttons').children(':not([data-switch-to-slide])').each(function(){
        $(this).on('click', function(){
            var validClasses = ['next', 'prev', 'home'];
            var classes = $(this).attr('class').split(' ');
            var selectedClass = null;
            _.forEach(classes, function(v, k){
                if(_.includes(validClasses, v)){
                    selectedClass = v;
                }
            })

            if(_.isString(selectedClass)){
                presentation.switchToSlide({
                    slide: selectedClass
                })
            }
        })
    })
}