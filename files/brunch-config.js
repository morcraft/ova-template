const _ = require('lodash')
const ovaServerTools = require('./ovaServerTools.js')
const courseData = require('./app/courseData.json');
exports.config = {
  // See http://brunch.io/#documentation for docs.

  npm: {
    globals: {
      $: 'jquery',
        jQuery: 'jquery',
    },
        styles: {
            highlightjs: ['styles/agate.css']
        }
  },

  modules: {
    nameCleaner(path) { return path.replace(/^app\/(?:scripts\/)?/, ''); },
    autoRequire: {
      'js/app.js': ['init']
    }
  },
  hooks: {
    preCompile() {
      
      console.log('About to compile')
      //console.log("About to compile...");
      ovaServerTools.writeImageSources();
      //ovaServerTools.writeFontSources();
    },
    onCompile(generatedFiles, changedAssets) {
      //console.log("Compiled.");
      // //console.log(generatedFiles.map(f => f.path));
    }
  },
  server: {
    run: true
  },

  files: {
    javascripts: {
      joinTo: {
        'js/jquery.js': /node_modules\/jquery\/dist\/jquery\.js/,
        'js/app.js': /^app/,
        'js/vendor.js':  /^(?!app|node_modules\/jquery\/dist\/jquery\.js)/
      }
    },
    stylesheets: {
      joinTo: {

          'css/wtf.css': [
                  /^(app[\\/]scss[\\/]styles(?!-rtl))/,
                  /node_modules[\\/](?!bootstrap-rtl[\\/]dist[\\/]css[\\/]bootstrap-rtl.css)/,
          ],
          'css/app.css':[
                  /^(app[\\/]scss[\\/]styles(?!-rtl))/,
                  /node_modules[\\/](?!bootstrap-rtl[\\/]dist[\\/]css[\\/]bootstrap-rtl.css)/
          ],
          'css/app-rtl.css': [
                  /^(app[\\/]scss[\\/]styles-rtl)/,
                  /(node_modules[\\/]bootstrap-rtl[\\/]dist[\\/]css[\\/]bootstrap-rtl.css)/
          ],
          'css/editor.css':[
                  /^(app[\\/]scss[\\/]editor(?!-rtl))/
          ],
          'css/editor-rtl.css': [
                  /^(app[\\/]scss[\\/]editor-rtl)/
          ]

      }
    },
    // something that probabaly needs to be addressed in brunch-static/brunch core
    // see https://github.com/bmatcuk/brunch-static/blob/master/src/brunch-static.coffee#L8
    // /\.static\.\w+$/ should not wrapped
    templates: {
        joinTo: 'js/templates.js'
      }
  },
        
  conventions: {
      // we don't want javascripts in asset folders to be copied like the one in
      // the bootstrap assets folder
      assets: /assets[\\/](?!javascripts)/
    },

  plugins: {

    handlebars: {
      // overrides: handlebars => {
      //   handlebars.JavaScriptCompiler.prototype.nameLookup = (parent, name, type) => {
      //     // Your custom nameLookup method.
      //   }
      // },
      include: {
        runtime: false, // include the full compiler javascript,
        amd: false
      },
      pathReplace: /0^/,
      //pathReplace: /0^/, // match nothing, use full file path for module name
      locals: courseData
    },
    
    static: {
      processors: [
        // require('html-brunch-static')({
        //   handlebars: {
        //     enableProcessor:true
        //   }
        // })
      ]
    },
      
    sass: {
      debug: 'comments', // or set to 'debug' for the FireSass-style output
      mode: 'native', // set to 'ruby' to force ruby
      allowCache: true,
      options: {
        includePaths: ['node_modules']
      }
    },

    assetsmanager: {
      copyTo: {
        'fonts': ['node_modules/bootstrap-sass/assets/fonts/bootstrap*', 'app/fonts/*', 'node_modules/font-awesome/fonts/*'],
        'img': ['app/img/*'],
        'common': ['app/common/*'],
        'dynamicPdf': ['app/dynamicPdf/*'],
        './': ['app/rootFiles/*']
      }
    }
  }
};

